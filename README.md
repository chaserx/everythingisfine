# everything is fine. lol

## Description

I need to be better about the capture, collection, curation, and contemplation of information that may be valuable to my future self. The intent here was to work on synthesizing knowledge as I go from my time as an manager and other roles.

## Setup

This project uses [docsy](https://www.docsy.dev/docs/get-started/docsy-as-module/start-from-scratch/#detailed-setup-instructions), a theme for [Hugo](https://gohugo.io/) to process Markdown files into a static site. This theme is used frequently by for documentation.

1. You'll need to have Go installed. Install via [golang asdf plugin](https://github.com/kennyp/asdf-golang)
2. You'll also need a modern version of NodeJs. Install via [nodejs asdf plugin](https://github.com/asdf-vm/asdf-nodejs)
3. `asdf install` to install version from `.tool-versions`
4. Install node packages `npm install`
5. You'll need to install Hugo from Homebrew. `brew install hugo`

## Running locally

- `hugo server`


## GitLab Pages

See [.gitlab-ci.yml](.gitlab-ci.yml) for build and deploy pipeline configuration details.

With each commit to the `main` branch, a pipeline will be triggered to build the rendered site into a public artifact directory which is picked up by GitLab pages.
