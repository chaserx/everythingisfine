---
title: "Manager Map of Content"
date: 2022-10-20T00:30:33-04:00
draft: false
---

# Manager Resources

- [The Manager's Handbook](https://themanagershandbook.com/the-highlights)  => [[manager's handbook highlights]]
- [Tools for Better Thinking](https://untools.co/)  contains some guides and template both PDF and Notion compatible for techniques to think about problems well
- [5 Engineering Manager Archetypes](https://www.patkua.com/blog/5-engineering-manager-archetypes/) => I am probably most likely aligned with the Archetype 2: The Team Lead EM, but Dan would like me to be more like Archetype 3: The Delivery EM
- [Managing 2020](https://managing2020.com/) => It's a hard time to be a manager. $99 video series
- [The OODA loop and the half-beat](https://thestrategybridge.org/the-bridge/2020/3/17/the-ooda-loop-and-the-half-beat) => Observe-Orient-Decide-Act loop
- [What is an engineering manager anyway](https://dev.to/solidi/what-is-an-engineering-manager-anyway-4and)
- [bias for action](https://thehumanfactor.biz/do-you-have-a-bias-for-action/) on the negative consquesnces of bias for action: ` bias for action frequently leads to bad decisions and even worse results`
- [managing up](https://hbr.org/2015/01/what-everyone-should-know-about-managing-up) methods dependent on different kinds of bosses. universal: understand what makes your boss tick and figuring out how to be a genuine source of help
- [A Tactical Guide to Managing Up](https://firstround.com/review/a-tactical-guide-to-managing-up-30-tips-from-the-smartest-people-we-know/) ==this one has practical tips==
- [Why managing up is a skill set you need](https://www.forbes.com/sites/rodgerdeanduncan/2018/05/26/why-managing-up-is-a-skillset-you-need/?sh=463c3fda37fd) => interview with some tips at the end.
- [Recommended Management Books](https://caitiem.com/2020/12/28/recommended-engineering-management-books/)
- [Leadership Library for Engineers](https://leadership-library.dev/The-Leadership-Library-for-Engineers-c3a6bf9482a74fffa5b8c0e85ea5014a)
- [[Productivity Resources]] <<--- fix obsidian reference
- [Maximizing Developer Effectiveness](https://martinfowler.com/articles/developer-effectiveness.html) full of case studies and advice on how to get started. => micro feedback loops; optimize for organization effectiveness => It starts with a recognition by leadership that technology — and removing friction from development teams — is vital to the business. ==really dig into this one; break out into it's own document==
